# SPLITTER TOOL

This tool is to split a wel file or rch file between two differnt nrds in the WWUM model. The tool enables the user to select which cells they'd like to replace by using a csv file to determine the replacements. It outputs either a wel file or rch file from the results of the operations.

## How to install
Go to a spot on your computer that you'd like to work with the code. Use a web browser and navigate to: https://github.com/AdaptiveResourcesInc/splitter/releases/latest and download the zip file to your computer and then extract it to the location where you want the program to live. Then use the terminal to cd into the folder where you'll find a requirements.txt file. Run the following command in terminal: `pip install -r requirements.txt`. This will install the required python packages to your computer. Installation is complete.

## How to use the tool
To use the tool make sure your files that you'd like to split are inside the "input_files" dir. Then use the terminal and cd into the "splitter" folder that contains the splitter.py file. Once there you can use the following command to run the program: `python splitter.py -h` will show you the help and list the functions. The function `python splitter.py rch` will start the rch file creation process. The function `python splitter.py wel` will start the wel file creation process. Follow the prompts after the intial command to finish the file creation.

## Cell Addresses
The WWUM has 476 rows and 520 columns. To better enable the programming to handle the cells, an address was created for each cell. The address is calculated as follows: `(row - 1) * 520 + clm = address`. This allows for each cell in the model to have a unique integer value. That is the addresses used in the nrd_cells.csv file to determine the replacment.

## nrd_cells.csv
The csv file is a list of the cells that are in each nrd or can be any area that you'd like to replace one file with another. The file name can be entered within the program, so many files can be saved for multiple operations. The file is organized by `celladdress,selection`. The cell address is defined above. The selection is the replacement flag. If the selection = 1 then the baseline file is used. If selection = 2 then the replacement file is used. If the cell is not found listed in this file, it defaults to baseline values.

## Baseline Files
These are files that have the values tha are not being replaced. The result will be the values from this file unless the cell address = 2 explained above. Please include the file extension when typing in this file (i.e. .wel or .rch).

## Replacement Files
These are the files that when the _cells.csv file identifies them as a replacement cell, the values from these files will be used for the output if flagged = 2.

## Output files
### WEL File Process Assumptions
The wel file has a few assumptions. First it is assumed that all pumping values are going to be negative. If there are positive values they will not be included in the final output regardless of the flags. The second is that any wells that have zero pumping in that stress period will be be included in the final output file. They are being output via Flopy in MODFLOW 2K format.

### RCH File Process Assumptions
The RCH file can have either positive or negative values. The file format is free and is being output by Flopy in MODFLOW 2K format.

