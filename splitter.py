import csv
import sys
from cement.core.foundation import CementApp
from cement.core.controller import CementBaseController, expose
from datetime import datetime
from cement.utils.misc import init_defaults
import flopy
import numpy
import zipfile
from os import path
from progress.bar import Bar
from flopy.modflow import Modflow, ModflowWel, ModflowDis, ModflowRch
import pathlib
import logging


# define our default configuration options
defaults = init_defaults('myapp', 'log.logging')
defaults['myapp']['debug'] = False
defaults['myapp']['some_param'] = 'some value'
VERSION = '0.5.0'
BANNER = 'WWUM Splitter Tool v{0} Copyright (c) {1} Longitude 103'.format(VERSION, datetime.now().year)


# define any hook functions here
def my_cleanup_hook(app):
    pass


# define application controllers
class MyAppBaseController(CementBaseController):
    class Meta:
        label = 'base'
        description = "WWUM Splitter Tool"
        arguments = [
            (['--base-opt'], dict(help="option under base controller")),
            (['-v', '--version'], dict(action='version', version=BANNER)),
            (['-r'], dict(action='store', help='recharge file split')),
        ]

    @expose(help="default command for splitter tool", hide=True)
    def default(self):
        print(BANNER)
        print("Welcome to the splitter tool program, please select an option or find them under the -h")
        if self.app.pargs.sw:
            print('At default')

    @expose(help="split rch file")
    def rch(self):
        print("Split rch file")
        rchfile()

    @expose(help="split wel file")
    def wel(self):
        print("Split wel file")
        wel()


# define the application class
class MyApp(CementApp):
    class Meta:
        label = 'SplitterTool'
        config_defaults = defaults
        extensions = []
        hooks = [
            ('pre_close', my_cleanup_hook),
        ]
        handlers = [MyAppBaseController]


def main():
    with MyApp() as app:
        app.setup()
        logging.basicConfig(filename='splitter.log', format='%(asctime)s %(message)s', level=logging.INFO)
        logging.info('Started application')
        app.run()
        logging.info('Exit application')


def rchfile():
    stressp = int(input('How many stress periods are there? '))
    if stressp <= 0:
        stressp = 1

    fn = input('What do you want to name the output .rch file [testout is default]? ')
    if fn == '':
        fn = 'testout'

    filedir = path.dirname(path.realpath('__file__'))
    input_filedir = path.join(filedir, 'input_files/')

    bl_file = input('What is the name of the baseline file in input_files folder? ')
    rp_file = input('What is the name of the replacement values file in the input_files folder? ')
    nrd_cell = input('What is the name of the key file for the cells? ')
    if input('Want a zip file created? Y or n [n is default]') == 'Y':
        zp = True
    else:
        zp = False

    # there are 24752 text lines per stress period, 520 columns, 476 rows
    # there are 52 lines per MODFLOW model row
    lst = [0]*247521
    out = []
    end_loop_break = False
    bar1 = Bar('Process read ', max=stressp)
    logging.info('RCH File Created: stress periods={0}; output_fn={1}; baseline_fn={2}; replace_fn={3}; nrd_cells={4}; zipfile={5}'.format(str(stressp), fn, bl_file, rp_file, nrd_cell, zp))

    # key file that will set which file values to use. If cell address is not present or value of cellid = 1 use
    # baseline.csv, otherwise use test_p97 file.
    with open(path.join(input_filedir, nrd_cell)) as csvfile:
        reader = csv.reader(csvfile)
        for item in reader:
            lst[int(item[0])] = int(item[1])

    # two files that are used for data
    with open(path.join(input_filedir, bl_file), 'r') as b, open(path.join(input_filedir, rp_file), 'r') as c:
        for x in range(3):  # skip the first 3 lines that are the file header
            b.readline()
            c.readline()

        while True:  # loop until end of file, this should loop here 1,328 times
            bar1.next()
            if end_loop_break: break
            tcel = 0
            for x in range(2):  # skip the first 2 lines that are the stress period header
                b.readline()
                c.readline()

            for rw in range(1, 477):
                if end_loop_break: break

                for cl in range(52):
                    # read both files at the same time to get the different data and split the 10 values in the row
                    b_row = b.readline().split()
                    c_row = c.readline().split()

                    if not b_row:
                        end_loop_break = True
                        break

                    for x in range(1, 11):

                        if lst[cl * 10 + x + tcel] == 1:  # cell address value == 1
                            out.append(b_row[x - 1])
                        elif lst[cl * 10 + x + tcel] == 2:  # cell address value == 2
                            out.append(c_row[x - 1])
                        else:
                            out.append(b_row[x - 1])

                tcel += 520
        bar1.finish()

    counter = 0
    rowlist = []
    rchdata = []
    rech = {}
    filedir = path.dirname(path.realpath('__file__'))
    pathlib.Path(path.join(filedir, 'output_files/')).mkdir(exist_ok=True)
    output_filedir = path.join(filedir, 'output_files/')
    # fn = 'testout'
    bar2 = Bar('Write RCH', max=stressp)

    for z in range(stressp):  # 2 for testing 1328 for production, might get it from user
        bar2.next()
        for x in range(1, 477):
            for y in range(520):
                rowlist.append(float(out[(y) + counter]))
            rchdata.append(list(rowlist))  # you must use the list() function to create a new list otherwise its just a reference.
            del rowlist[:]
            counter += 520

        try:
            rech[z] = numpy.array(list(rchdata), numpy.float)
        except ValueError:
            print('error in process at: stress period: {0} and row: {1}'.format(str(z+1), str(x)))
            with open(path.join(output_filedir, 'error.csv'), 'w') as el:
                writer = csv.writer(el)
                writer.writerows(rchdata)
            print('error logged in output')
            sys.exit(1)
        else:
            del rchdata[:]
    bar2.finish()

    m = flopy.modflow.Modflow(fn, version='mf2k', model_ws=output_filedir)

    nlay = 1  # one layer
    nrow = 476  # 476 rows
    ncol = 520  # 520 columns
    nper = stressp  # 5 stress periods

    print('Writing RCH file.')
    #  dis file required for rch file creation.
    dis = ModflowDis(m, nlay=nlay, nrow=nrow, ncol=ncol, nper=nper)
    rch = ModflowRch(m, ipakcb=50, rech=rech)
    rch.write_file()  # writes out rch file

    if zp:
        print('The {0}.zip file is being created'.format(fn))
        #  Creates a zip file
        with zipfile.ZipFile(path.join(output_filedir, fn + '.zip'), 'w', zipfile.ZIP_DEFLATED) as zf:
            zf.write(path.join(output_filedir, fn + '.rch'), arcname=fn + '.rch')

    logging.info('All Files created')
    print('The rch file called {0} is finished'.format(fn))
    # remove(path.join(output_filedir, fn + '.rch'))


def wel():
    stressp = int(input('How many stress periods are there? '))
    if stressp <= 0:
        stressp = 1

    fn = input('What do you want to name the output .wel file [testout_wel is default]? ')
    if fn == '':
        fn = 'testout_wel'

    filedir = path.dirname(path.realpath('__file__'))
    input_filedir = path.join(filedir, 'input_files/')
    pathlib.Path(path.join(filedir, 'output_files/')).mkdir(exist_ok=True)
    output_filedir = path.join(filedir, 'output_files/')

    bl_file = input('What is the name of the baseline file in input_files folder? ')
    rp_file = input('What is the name of the replacement values file in the input_files folder? ')
    nrd_cell = input('What is the name of the key file for the cells? ')
    if input('Want a zip file created? Y or n [n is default]') == 'Y':
        zp = True
    else:
        zp = False

    logging.info('WEL File Created: stress periods={0}; output_fn={1}; baseline_fn={2}; replace_fn={3}; nrd_cells={4}; zipfile={5}'.format(str(stressp), fn, bl_file, rp_file, nrd_cell, zp))

    bar1 = Bar('Process read ', max=stressp)
    sp = 0
    weldata = {}
    wllst = zerolistmaker(476 * 520)
    cnt = 0

    # load the cells into a dict
    lst = {}
    with open(path.join(input_filedir, nrd_cell), 'r') as cells:
        reader = csv.reader(cells)
        for item in reader:
            lst[int(item[0])] = int(item[1])

    # load the wel files
    with open(path.join(input_filedir, bl_file), 'r') as b, open(path.join(input_filedir, rp_file), 'r') as c:
        for x in range(3):  # skip the first 3 lines that are the file header
            b.readline()
            c.readline()

        while True:
            # this is the header for the stress period
            sph = b.readline()
            if not sph: break
            weldata[sp] = [] # set the data def for the dict to lst, so you can append
            bar1.next()
            num_wells = int(sph[:10])

            # now interate through the wells
            for y in range(num_wells):
                line_a = b.readline()  # read a line of the wel file
                # line_b = c.readline()
                rw_a = int(line_a[11:20])  # assign row
                cl_a = int(line_a[21:30])  # assign column
                pump_a = float(line_a[30:40])  # assign pumping amount
                # pump_b = float(line_b[30:40])

                cellid = (rw_a - 1) * 520 + cl_a

                if cellid in lst:
                    if lst[cellid] != 2:
                        wllst[cellid] = pump_a
                else:
                    wllst[cellid] = pump_a

            # this is the header for the stress period
            sph_b = c.readline()
            # if not sph: break
            # weldata[sp] = [] # set the data def for the dict to lst, so you can append
            num_wells = int(sph_b[:10])

            # now interate through the wells
            for y in range(num_wells):
                # line_a = b.readline()  # read a line of the wel file
                line_b = c.readline()
                rw_b = int(line_b[11:20])  # assign row
                cl_b = int(line_b[21:30])  # assign column
                # pump_a = float(line_a[30:40])  # assign pumping amount
                pump_b = float(line_b[30:40])

                cellid_b = (rw_b - 1) * 520 + cl_b

                if cellid_b in lst:
                    if lst[cellid_b] == 2:
                        wllst[cellid_b] = pump_b

            # iterate through the list and build dict
            for x in wllst:
                if x < 0:
                    weldata[sp].append([0, cnt // 520 + 1, cnt - ((cnt // 520) * 520), x])
                cnt += 1
            sp += 1
            wllst = zerolistmaker(476 * 520)
            cnt = 0

        bar1.finish()

    print('Writing {0} wel file.'.format(fn))
    m = Modflow(fn, version='mf2k', model_ws=output_filedir)
    wel = ModflowWel(m, 50, weldata)
    wel.write_file()

    if zp:
        print('Creating zip file.')
        #  Creates a zip file
        with zipfile.ZipFile(path.join(output_filedir, fn + '.zip'), 'w', zipfile.ZIP_DEFLATED) as zf:
            zf.write(path.join(output_filedir, fn + '.wel'), arcname=fn + '.wel')

    logging.info('All Files created')
    print('Finished Creating {0}.wel file'.format(fn))


def zerolistmaker(n):
    listofzeros = [0] * n
    return listofzeros


if __name__ == '__main__':
    main()
